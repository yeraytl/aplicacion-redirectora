#!/usr/bin/python

#
# Simple HTTP Server
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2010
# September 2009
# Febraury 2022


import socket
import random

urls = ["https://www.google.com", "https://www.yahoo.com", "https://www.bing.com"]

# Función para generar una respuesta de redirección aleatoria
def generate_redirect():
    new_url = random.choice(urls)
    response = "HTTP/1.1 302 Found\r\n"
    response += f"Location: {new_url}\r\n\r\n"
    return response.encode('ascii')

# Create a TCP objet socket and bind it to a port
# We bind to 'localhost', therefore only accepts connections from the
# same machine
# Port should be 80, but since it needs root privileges,
# let's use one above 1024

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as mySocket:
    mySocket.bind(('', 1235))

    # Queue a maximum of 5 TCP connection requests
    mySocket.listen(5)

    # Accept connections, read incoming data, and answer back an HTLM page
    #  (in a loop)
    while True:
        print("Waiting for connections")
        (recvSocket, address) = mySocket.accept()
        with recvSocket:
            print("HTTP request received:")
            request = recvSocket.recv(2048)
            print(request)
            # Si la solicitud incluye una redirección, se genera una redirección aleatoria
            #if b"GET /redirect" in request:
            response = generate_redirect()
            # De lo contrario, se envía una respuesta de OK
            #else:
            #    response = b"HTTP/1.1 200 OK\r\n\r\n<h1>Hello World!</h1>"
            recvSocket.send(response)
