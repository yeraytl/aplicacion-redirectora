# Aplicación Redirectora

Repositorio de plantilla para el ejercicio "Aplicación Redirectora". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.

En este ejercicio vamos a crear una aplicación redirectora que nos redirija a una pagina aleatoria escogida de una sere de URLs que hemos 
añadido a una lista. Por ejemplo en mi programa me he creado la siguiente lista:
	urls = ["https://www.google.com", "https://www.yahoo.com", "https://www.bing.com"]
	
El programa lo que hará será elegir con random.choice una de las 3 urls que tenemos dentro de la lista y redirigirnos hacia esta.

